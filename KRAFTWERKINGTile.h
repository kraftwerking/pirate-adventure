//
//  KRAFTWERKINGTile.h
//  Pirate Adventure
//
//  Created by RJ Militante on 1/20/15.
//  Copyright (c) 2015 Kraftwerking LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KRAFTWERKINGArmor.h"
#import "KRAFTWERKINGWeapon.h"

@interface KRAFTWERKINGTile : NSObject

@property (strong, nonatomic) NSString *story;
@property (strong, nonatomic) UIImage *backgroundImage;
@property (strong, nonatomic) NSString *actionButtonName;

@property (strong, nonatomic) KRAFTWERKINGArmor *armor;
@property (strong, nonatomic) KRAFTWERKINGWeapon *weapon;
@property (nonatomic) int healthEffect;

@end
