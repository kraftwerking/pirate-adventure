//
//  KRAFTWERKINGAppDelegate.h
//  Pirate Adventure
//
//  Created by RJ Militante on 1/19/15.
//  Copyright (c) 2015 Kraftwerking LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KRAFTWERKINGAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
