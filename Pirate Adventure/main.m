//
//  main.m
//  Pirate Adventure
//
//  Created by RJ Militante on 1/19/15.
//  Copyright (c) 2015 Kraftwerking LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "KRAFTWERKINGAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([KRAFTWERKINGAppDelegate class]));
    }
}
