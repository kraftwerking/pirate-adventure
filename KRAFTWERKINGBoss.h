//
//  KRAFTWERKINGBoss.h
//  Pirate Adventure
//
//  Created by RJ Militante on 1/21/15.
//  Copyright (c) 2015 Kraftwerking LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KRAFTWERKINGBoss : NSObject

@property (nonatomic) int health;

@end
