//
//  KRAFTWERKINGArmor.h
//  Pirate Adventure
//
//  Created by RJ Militante on 1/20/15.
//  Copyright (c) 2015 Kraftwerking LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KRAFTWERKINGArmor : NSObject

@property (strong, nonatomic) NSString *name;
@property (nonatomic) int health;

@end
