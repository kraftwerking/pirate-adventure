//
//  KRAFTWERKINGFactory.h
//  Pirate Adventure
//
//  Created by RJ Militante on 1/20/15.
//  Copyright (c) 2015 Kraftwerking LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KRAFTWERKINGCharacter.h"
#import "KRAFTWERKINGBoss.h"

@interface KRAFTWERKINGFactory : NSObject

-(NSArray *)tiles;
-(KRAFTWERKINGCharacter *)character;
-(KRAFTWERKINGBoss *)boss;

@end
